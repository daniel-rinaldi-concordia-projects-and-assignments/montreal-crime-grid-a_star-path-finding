# COMP 472 Assignment 1

- [COMP 472 Assignment 1](#comp-472-assignment-1)
  - [Getting Started](#getting-started)
    - [Install Libraries](#install-libraries)
    - [Run](#run)
      - [In Terminal](#in-terminal)
      - [How to operate](#how-to-operate)
  - [Authors](#authors)
  - [Grading Scheme](#grading-scheme)
    - [Map](#map)
    - [A* Heuristic Method](#a-heuristic-method)
    - [Optimal Path](#optimal-path)
    - [Code Quality](#code-quality)
    - [README](#readme)

In this assignment, you will generate the crime risk map based on the crime rates and
implement an A* heuristic search algorithm to find an optimal path between two
coordinates on the map.

See `COMP472 Assignment1.pdf` for more details.

## Getting Started

### Install Libraries

`pip install pyshp`  
`pip install matplotlib`

### Run
#### In Terminal
- `cd` into the project root directory (same directory that this file is in).  
- `python src/main.py`  

#### How to operate
- Select `Crime Grid` (option 1) from main menu and enter a cell size when prompted.  

Note: For the data provided by the shp file given to us in the assignment, any cell size >= 0.04 results in the all the data being in one cell. This is **not an error**. This is because that cell size is bigger than the bounding box of the geolocation data.  
Suggested values for nicer results are : `[0.002, 0.003, 0.004, 0.005]`  

## Authors

- **Daniel Rinaldi** - 40010464

## Grading Scheme
**Note:** The word grid is used in the assignment handout to refer to an individual square cell. I use the latter terminology in my assignment.

| Grading Criteria     | Points |
| ----------------     | ------:|
| Map                  | 5      |
| A* Heuristic Method  | 10     |
| Optimal Path         | 3      |
| Code Quality         | 1      |
| README               | 1      |
| Total                | 20     |

### Map

- Draw the grids **[1 pt]**
- Generate the map by crime rates **[2 pt]**
- Apply different threshold by prompting user for inputs **[1 pt]**
- Display the number of total crimes in each grid, average and standard deviation of all grids **[1 pt]**

### A* Heuristic Method

- Heuristic function, total cost, admissibility **[4 pt]**
- Follow the rules in 2.1.5 to design heuristic method **[6 pt]**

### Optimal Path

- Find the path from any 2 points by prompting user for inputs **[2 pt]**
- Execution time and instant outputs **[1 pt]**

### Code Quality

- Necessary comments, readability and clarity **[1 pt]**

### README

- Instructions on how to run your program and list of libraries **[1 pt]**
