# --------------------------------------------------------
# Assignment 1
# Written by Daniel Rinaldi 40010464
# For COMP 472 Section JX – Summer 2020
# --------------------------------------------------------

import sys

from app import App

def main(args: list):
    App().start()

if __name__ == "__main__":
    main(sys.argv)
else:
    main()