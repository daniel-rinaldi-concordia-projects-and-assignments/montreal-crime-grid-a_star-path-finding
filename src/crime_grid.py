# --------------------------------------------------------
# Assignment 1
# Written by Daniel Rinaldi 40010464
# For COMP 472 Section JX – Summer 2020
# --------------------------------------------------------

import os
import math
import datetime
import enum
import heapq
from collections import OrderedDict

import shapefile
import numpy

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as pltcolors
import matplotlib.ticker as pltticker
import matplotlib.widgets as pltwidgets

import constants

# specify the backend for matplotlib to use for the gui
matplotlib.use('TkAgg')

class CrimeGrid:
    class Mode(enum.Enum):
        Default = 1
        Painter = 2
        Eraser = 3
    
    def __init__(self):
        self.is_loaded = False
        self.max_crime_count = 0
        self.mean = 0
        self.std_dev = 0
        self.colors = ['#fafafa', '#212121', '#1565C0', '#1E88E5', '#607D8B', '#f44336', '#4CAF50', '#FFEB3B', '#4FC3F7', '#81C784']
        
        self._cell_size = None
        self._threshold = 0.5
        self._shp = None
        self._crime_grid = None
        self._crime_rate_treshold = 0
        self._figure = None
        self._grid_axes = None
        self._threshold_axes = None
        self._ai_path_markers = []
        self._error_markers = []
        self._ai_solution_path = []
        self._plotted_points = []
        self._is_drawing = False
        self._info_text_object = None
        self._dynamic_info_text = ''
        self._mode = CrimeGrid.Mode.Default
        
        self._debug = True
        self._debug_options = { 'draw_open_closed_list': True, 'draw_attempted_paths': True, 'enable_painting': True }


    """
        load
        loads the data from the given .shp file and creates a crime grid with square cells of size {cell_size}

        Arguments:
            shp_file_path {str} -- the path to the .shp file that needs to be loaded
            cell_size {float} -- the size of each edge of a square cell in the grid
            bbox {list} -- the bounding box to use for the map, or None to take it from the .shp file, defaults to None
    """
    def load(self, shp_file_path: str, cell_size: float, bbox: list = None):
        # set the cell_size
        self._cell_size = cell_size
        
        # read encoding from provided file (defaults to 'ISO-8859-1' if there is a file read problem)
        codepage = 'ISO-8859-1'
        if os.access(shp_file_path + ".cpg", os.R_OK):
            cpg_file = open(shp_file_path + ".cpg", 'r')
            codepage = cpg_file.readline().strip()
            cpg_file.close()
        
        # read shp file with proper encoding
        self._shp = shapefile.Reader(shp_file_path, encoding=codepage)
        
        # set bbox
        self._bbox = bbox if bbox != None else self._shp.bbox
        
        # print shp file info
        print('\n', self._shp)
        print("    Bounding Box:", self._bbox, '\n')
        
        # extract bounds
        xMin, yMin, xMax, yMax = self._bbox
        
        # calculate rows and columns based off cell size
        self._rows = math.ceil(numpy.float32(xMax - xMin) / self._cell_size)
        self._columns = math.ceil(numpy.float32(yMax - yMin) / self._cell_size)
        
        # clamp axes limits
        xMax = xMax - ((xMax - xMin) - (self._columns * self._cell_size))
        self._bbox[2] = xMax
        yMax = yMax - ((yMax - yMin) - (self._rows * self._cell_size))
        self._bbox[3] = yMax
        
        # construct crime count matrix
        self._crime_grid = numpy.array([[0]*(self._rows)]*(self._columns))
        self.max_crime_count = 0
        shape_records = self._shp.shapeRecords()
        for i in range(len(shape_records)):
            x, y = self._world_to_raster_space(
                (
                    float(shape_records[i].shape.__geo_interface__["coordinates"][0]), 
                    float(shape_records[i].shape.__geo_interface__["coordinates"][1])
                )
            )
            
            self._crime_grid[int(x)][int(y)] = self._crime_grid[int(x)][int(y)] + 1
            if self.max_crime_count < self._crime_grid[int(x)][int(y)]:
                self.max_crime_count = self._crime_grid[int(x)][int(y)]
        
        # remove toolbar options
        matplotlib.backend_bases.NavigationToolbar2.toolitems = (
            ('Home', 'Reset original view', 'home', 'home'),
            (None, None, None, None),
            (None, None, None, None),
            (None, None, None, None),
            (None, None, None, None),
            (None, None, None, None),
            ('Subplots', 'Configure subplots', 'subplots', 'configure_subplots'),
            (None, None, None, None),
            ('Save', 'Save the figure', 'filesave', 'save_figure')
        )
        
        # configure figures, plots and axes
        self._figure = plt.figure(figsize=(8, 8))
        
        self._grid_axes = plt.subplot2grid((100, 100), (0, 0), colspan=90, rowspan=74)
        self._threshold_axes = plt.subplot2grid((100, 100), (6, 97), colspan=3, rowspan=65)
        
        if not self._debug:
            self._info_axes = plt.subplot2grid((100, 100), (83, 0), colspan=100, rowspan=16)
        else:
            self._info_axes = plt.subplot2grid((100, 100), (83, 0), colspan=75, rowspan=16)
            self._save_button_axes = plt.subplot2grid((100, 100), (83, 76), colspan=24, rowspan=4)
        
        self._grid_axes.xaxis.set_ticks(numpy.arange(xMin, xMax + self._cell_size/10000, self._cell_size))
        self._grid_axes.yaxis.set_ticks(numpy.arange(yMin, yMax + self._cell_size/10000, self._cell_size))
        self._grid_axes.xaxis.set_major_formatter(pltticker.FormatStrFormatter('%0.3f'))
        self._grid_axes.yaxis.set_major_formatter(pltticker.FormatStrFormatter('%0.3f'))
        for i, label in enumerate(self._grid_axes.xaxis.get_ticklabels()):
            if i != 0 and i % 5 != 0 and i != self._columns:
                label.set_visible(False)
                self._grid_axes.xaxis.majorTicks[i].length = 0
        for i, label in enumerate(self._grid_axes.yaxis.get_ticklabels()):
            if i != 0 and i % 2 != 0 and i != self._rows:
                label.set_visible(False)
        self._grid_axes.set_xlabel("Cell Size: {:g}".format(self._cell_size), labelpad=8, backgroundcolor=self.colors[4], color=self.colors[0], fontweight='bold')
        self._grid_axes.set_title(
            "Use the threshold slider on the right to adjust threshold.\nClick on the graph to chose a starting and end points!", 
            fontsize=12, horizontalalignment='center'
        )    
        
        # grid lines serve as a visual aid however they may appear slightly off (by a few pixels) depending on user's aspect ratio / window size
        self._grid_axes.grid(which='both', axis='both', linestyle=':')
        
        self._info_axes.tick_params(which='both', axis='both', bottom=False, left=False, labelbottom=False, labelleft=False)
        self._info_axes.spines['top'].set_visible(False)
        self._info_axes.spines['left'].set_visible(False)
        self._info_axes.spines['right'].set_visible(False)
        self._info_axes.spines['bottom'].set_visible(False)
        self._info_axes.set_facecolor(self.colors[1])
        
        self._update_info_text()
        self._update_grid()
        
        self._threshold_axes.set_facecolor(self.colors[1])
        self._threshold_slider = pltwidgets.Slider(
            self._threshold_axes, "Threshold\n[0, {}, {}]".format(self._crime_rate_treshold, self.max_crime_count), 
            0.0, 1.0, valinit=self._threshold, orientation='vertical', facecolor=self.colors[0], valstep=0.01
        )
        self._threshold_slider.label.set_fontsize(9)
        self._threshold_slider.valtext.set_fontsize(9)
        self._threshold_slider.valtext.set_color(self.colors[1])
        self._threshold_slider.on_changed(lambda val: self._on_threshold_change(val))
        
        if self._debug:
            self._save_button = pltwidgets.Button(self._save_button_axes, 'SAVE PNG', color=self.colors[2], hovercolor=self.colors[3])
            self._save_button.label.set_fontsize(9)
            self._save_button.label._color = self.colors[0]
            self._save_button.label._fontproperties._weight = 'bold'
            self._save_button.on_clicked(lambda event: self._save())
        
        self._figure.canvas.mpl_connect('button_press_event', lambda event: self._on_grid_click(event))
        self._figure.canvas.mpl_connect('key_press_event', self._on_key_press)
        self._figure.canvas.mpl_connect('key_release_event', self._on_key_release)
        
        self._figure.canvas.mpl_connect('close_event', self._on_window_close)
        
        # adjust subplots margins
        plt.subplots_adjust(bottom=0.01, top=0.90)
        
        # CrimeGrid is fully loaded
        self.is_loaded = True


    """
        _on_window_close
        window close handler for the figure's canvas

        Arguments:
            event -- object containing information about the event
    """
    def _on_window_close(self, event):
        self._is_drawing = False


    """
        _world_to_raster_space
        transforms a given coordinate in world space to raster space (image/grid)

        Arguments:
            raster_coord {tuple} -- the coordinate to transform to raster space

        Returns:
            tuple -- the coordinate in raster space
    """
    def _world_to_raster_space(self, world_coord: tuple) -> tuple:
        return (float(world_coord[0] - self._bbox[0]) / self._cell_size, float(world_coord[1] - self._bbox[1]) / self._cell_size)


    """
        _raster_to_world_space
        transforms a given coordinate in raster space to world space (longitude and latitude)

        Arguments:
            raster_coord {tuple} -- the coordinate to transform to world space

        Returns:
            tuple -- the coordinate in world space
    """
    def _raster_to_world_space(self, raster_coord: tuple) -> tuple:
        return (raster_coord[0] * self._cell_size + self._bbox[0], raster_coord[1] * self._cell_size + self._bbox[1])


    """
        _update_info_text
        update and redraw the info_text
    """
    def _update_info_text(self):
        if self._info_text_object != None:
            # remove the previous info text
            self._info_text_object.remove()
        else:
            # calculate mean and stdev of crime count matrix
            self.mean = self._crime_grid.mean()
            self.std_dev = self._crime_grid.std()
        
        # append dynamic text to the static text
        text = "Mean : {}, Standard Deviation : {}\n".format(round(self.mean, 4), round(self.std_dev, 4)) + self._dynamic_info_text
        
        # construct and redraw another info text object
        self._info_text_object = self._info_axes.text(
            0.02, 0.95, text, fontsize=10, transform=self._info_axes.transAxes, 
            ha='left', va='top', backgroundcolor=self.colors[1], color=self.colors[0]
        )
        self._figure.canvas.draw_idle()


    """
        _update_grid
        update and draw the crime_grid
    """
    def _update_grid(self):
        self._calculate_crime_threshold()
        
        # calculate color boundary norm using computed threshold crime value cutoff
        transposed_crime_grid = self._crime_grid.transpose()
        if self._crime_rate_treshold != 0:
            color_bounds = [0, self._crime_rate_treshold - 0.0001, self._crime_rate_treshold*2]
        else:
            color_bounds = [0, 0.0001, 3]
        cmap = pltcolors.ListedColormap([self.colors[0], self.colors[1]])
        norm = pltcolors.BoundaryNorm(color_bounds, cmap.N)
        
        # construct and redraw grid
        xMin, yMin, xMax, yMax = self._bbox
        self._grid_axes.imshow(
            transposed_crime_grid, aspect='auto', origin='lower', interpolation='none',
            extent=[xMin, xMax, yMin, yMax], cmap=cmap, norm=norm
        )
        
        self._figure.canvas.draw_idle()


    """
        _calculate_crime_threshold
        calculates the crime count to be used as the threshold crime value
    """
    def _calculate_crime_threshold(self):
        # flatten grid and sort ascending
        crime_counts = self._crime_grid.flatten()
        crime_counts.sort()
        
        # compute threshold crime value cutoff
        threshold_index = math.floor((len(crime_counts)) * self._threshold)
        if threshold_index == len(crime_counts):
            self._crime_rate_treshold = self.max_crime_count + 0.999
        elif (len(crime_counts) != 0):
            self._crime_rate_treshold = crime_counts[threshold_index]
        else:
            self._crime_rate_treshold = 0


    """
        open
        opens a new matplotlib window

        Arguments:
            title {str} -- the title for the window
    """
    def open(self, window_title = ""):
        if not self.is_loaded:
            return
        
        self._figure.canvas.set_window_title(window_title)
        
        plt.figure(self._figure.number)
        plt.show()


    """
        _on_key_press
        key_press handler for the figure's canvas

        Arguments:
            event -- object containing information about the event
    """
    def _on_key_press(self, event):
        if self._debug:
            if self._debug_options['enable_painting']:
                if event.key == 'shift' and self._mode == CrimeGrid.Mode.Default:
                    self._mode = CrimeGrid.Mode.Eraser
                elif event.key == 'control' and self._mode == CrimeGrid.Mode.Default:
                    self._mode = CrimeGrid.Mode.Painter


    """
        _on_key_release
        key_release handler for the figure's canvas

        Arguments:
            event -- object containing information about the event
    """
    def _on_key_release(self, event):
        if self._debug:
            if self._debug_options['enable_painting']:
                if event.key == 'shift' and self._mode == CrimeGrid.Mode.Eraser:
                    self._mode = CrimeGrid.Mode.Default
                elif event.key == 'control' and self._mode == CrimeGrid.Mode.Painter:
                    self._mode = CrimeGrid.Mode.Default


    """
        _on_threshold_change
        handler for the threshold slider

        Arguments:
            value -- the new value the slider has been set to
    """
    def _on_threshold_change(self, value):
        if self._threshold == round(value, 2):
            return
        
        self._is_drawing = False
        self._threshold = round(value, 2)
        
        self._calculate_crime_threshold()
        if self._crime_rate_treshold != 0:
            self._grid_axes.get_images()[0].set_norm(pltcolors.BoundaryNorm([0, self._crime_rate_treshold - 0.00001, self._crime_rate_treshold*2], 2))
        else:
            self._grid_axes.get_images()[0].set_norm(pltcolors.BoundaryNorm([0, 0.00001, 1], 2))
        
        self._threshold_slider.label.set_text("Threshold\n[0, {}, {}]".format(int(self._crime_rate_treshold), self.max_crime_count))
        self._dynamic_info_text = ''
        self._update_info_text()
        self._ai_solution_path = []
        self._remove_points_from_plot(self._ai_path_markers)
        self._remove_points_from_plot(self._error_markers)
        self._remove_points_from_plot(self._plotted_points)
        self._figure.canvas.draw_idle()


    """
        _save
        saves the figure to '{PROJECT_ROOT_DIR}/output/
    """
    def _save(self):
        plt.figure(self._figure.number)
        cells_size_file_str = str(self._cell_size).replace('.', '_')
        file_name = "debug-crime_grid" if self._debug else "crime_grid"
        plt.savefig(constants.PROJECT_ROOT_DIR + 'output/' + file_name + '-c' + cells_size_file_str + '-t' + str(self._threshold) + '.png')


    """
        _on_grid_click
        click handler for the figure's canvas

        Arguments:
            event -- object containing information about the event
    """
    def _on_grid_click(self, event):
        mouse_x, mouse_y = event.xdata, event.ydata
        if self._grid_axes == event.inaxes and mouse_x != None and mouse_y != None:
            if len(self._error_markers) > 0:
                self._remove_points_from_plot(self._error_markers)
            
            raster_pos = self._world_to_raster_space((mouse_x, mouse_y))
            
            if self._mode == CrimeGrid.Mode.Painter:
                self._crime_grid[int(raster_pos[0])][int(raster_pos[1])] = self.max_crime_count+1
                self._update_grid()
            elif self._mode == CrimeGrid.Mode.Eraser:
                self._crime_grid[int(raster_pos[0])][int(raster_pos[1])] = 0
                self._update_grid()
            else:
                node_pos = (int(round(raster_pos[0])), int(round(raster_pos[1])))
                node_world_pos = (
                    node_pos[0] * self._cell_size + self._bbox[0], 
                    node_pos[1] * self._cell_size + self._bbox[1]
                )
                
                if len(self._ai_path_markers) >= 2:
                    self._is_drawing = False
                    self._ai_solution_path = []
                    self._remove_points_from_plot(self._ai_path_markers)
                    self._remove_points_from_plot(self._plotted_points)
                    self._ai_path_markers.clear()
                    self._figure.canvas.draw_idle()
                
                marker_size = 6 if self._cell_size < 0.002 else 8
                
                if len(self._ai_path_markers) <= 0:
                    self._ai_solution_path.append(node_pos)
                    self._ai_path_markers.append(self._grid_axes.plot(node_world_pos[0], node_world_pos[1], marker='o', markerfacecolor=self.colors[6], markeredgecolor='k', markersize=marker_size+2, zorder=50))
                    self._figure.canvas.draw_idle()
                else:
                    self._ai_solution_path.append(node_pos)
                    self._ai_path_markers.append(self._grid_axes.plot(node_world_pos[0], node_world_pos[1], marker='*', markerfacecolor=self.colors[7], markeredgecolor='k', markersize=marker_size+4, zorder=50))
                    self._figure.canvas.draw_idle()
                    self._astar_heuristic_search()


    """
        _remove_points_from_plot
        removes points from the plot

        Arguments:
            points_list {tuple} -- the list of Line2d point objects to remove
    """
    def _remove_points_from_plot(self, points_list: list):
        if (len(points_list) > 0):
            for line2d_point in points_list:
                line2d_point.pop(0).remove()
        points_list.clear()


    """
        _check_node_surrounded
        checks if the node at the given position in the crime_grid is surrounded (blocked by a wall in all directions)

        Arguments:
            node_pos {tuple} -- the position of the node to check in grid coordinates

        Returns:
            bool -- true if the node at the given position in the crime_grid is surrounded, false otherwise
    """
    def _check_node_surrounded(self, node_pos: tuple) -> bool: 
        blocked_edges = [0, 0]
        
        if node_pos[0] >= self._columns:
            blocked_edges[0] = 1
        elif node_pos[0] <= 0:
            blocked_edges[0] = -1
        
        if node_pos[1] >= self._rows:
            blocked_edges[1] = 1
        elif node_pos[1] <= 0:
            blocked_edges[1] = -1
        
        return not bool(
            (blocked_edges[0] <= 0 and blocked_edges[1] <= 0 and self._crime_grid[node_pos[0]][node_pos[1]] < self._crime_rate_treshold) or 
            (blocked_edges[0] >= 0 and blocked_edges[1] <= 0 and self._crime_grid[node_pos[0] - 1][node_pos[1]] < self._crime_rate_treshold) or
            (blocked_edges[0] >= 0 and blocked_edges[1] >= 0 and self._crime_grid[node_pos[0] - 1][node_pos[1] - 1] < self._crime_rate_treshold) or 
            (blocked_edges[0] <= 0 and blocked_edges[1] >= 0 and self._crime_grid[node_pos[0]][node_pos[1] - 1] < self._crime_rate_treshold)
        )


    """
        _plot_line
        plots every point in points as a line onto the grid_axes

        Arguments:
            points {list} -- a list of 2d points representing a line to plot onto the grid_axes
            color {str} -- the color of the line (default is rainbow)
    """
    def _plot_line(self, points: list, color: str = None):
        if len(points) <= 1:
            return
        
        x_values = [self._raster_to_world_space(p)[0] for p in points]
        y_values = [self._raster_to_world_space(p)[1] for p in points]
        
        line_width = 1 if self._cell_size < 0.002 else 2
        
        if color == None:
            self._plotted_points.append(self._grid_axes.plot(x_values, y_values, linestyle='-', color='k', linewidth=line_width+1, solid_capstyle='round', solid_joinstyle='round'))
        
        self._is_drawing = True
        if color == None:
            # print rainbow line (slower but pretty)
            for i in range(len(points) - 1):
                xs = [x_values[i], x_values[i + 1]]
                ys = [y_values[i], y_values[i + 1]]
                if self._is_drawing:
                    if color == None:
                        self._plotted_points.append(self._grid_axes.plot(xs, ys, linestyle='-', linewidth=line_width, solid_capstyle='round', solid_joinstyle='round'))
                else:
                    break
        else:
            self._plotted_points.append(self._grid_axes.plot(x_values, y_values, linestyle='-', color=color, linewidth=line_width, solid_capstyle='round', solid_joinstyle='round'))
        
        self._figure.canvas.draw_idle()
    
    
    """
        _plot_points
        plots every point in points onto the grid_axes

        Arguments:
            points {list} -- a list of 2d points to plot onto the grid_axes
            color {str} -- the color used for the points
            marker {str} -- the marker to use
    """
    def _plot_points(self, points: list, color: str, marker: str):
        if len(points) <= 0:
            return
        
        x_values = [self._raster_to_world_space(p)[0] for p in points]
        y_values = [self._raster_to_world_space(p)[1] for p in points]
        
        marker_size = 6 if self._cell_size < 0.002 else 9
        
        self._is_drawing = True
        for i in range(len(points)):
            if self._is_drawing:
                self._plotted_points.append(self._grid_axes.plot(x_values[i], y_values[i], marker=marker, markersize=marker_size, color=color))
            else:
                break
        
        self._figure.canvas.draw_idle()


    """
        _astar_heuristic_search
        This function performs A* heuristic search on the crime_grid map and draws the solution path as a line on the _grid_axes
        Note:
            - uses a minheap data structure for the open list with heap nodes = (fn, h*, (x, y))
            - this algorithm uses h* as a tiebreaker when presented with equal cost paths
    """
    def _astar_heuristic_search(self):
        solution_found = False
        
        # initialize start node, goal node, and list of operations
        start_node = self._ai_solution_path[0]
        goal_node = self._ai_solution_path[len(self._ai_solution_path) - 1]
        operations = [(0,1),(0,-1),(1,0),(-1,0),(1,1),(1,-1),(-1,1),(-1,-1)]
        
        # initialize dictionary of actual costs of movement from start node to node n (keys)
        gn_dict = {start_node: 0}
        # initialize dictionary of total costs for node n (fn = h(n) + g(n))
        fn_dict = {start_node: self._heuristic(start_node, goal_node) + gn_dict[start_node]}
        # initialize dictionary that will be used to trace path
        path_dict = {start_node: None}
        
        # initialize open list (heapq) and closed list (OrderedDict for faster lookup while maintaining order)
        open_list_heap = []
        heapq.heappush(open_list_heap, (fn_dict[start_node], fn_dict[start_node], start_node))
        closed_odict = OrderedDict()
        
        start_time = datetime.datetime.now()
        self._dynamic_info_text = "[{:d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}.{:0004d}] Starting A* heuristic search...\n".format(
            start_time.year, start_time.month, start_time.day, start_time.hour, start_time.minute, start_time.second, start_time.microsecond
        )
        self._update_info_text()
        
        # the time above will be a little off because it needs to update the info text and redraw the axes, so we restart the timer here
        true_start_time = datetime.datetime.now()
        
        # A* heuristic search
        # ============================================================================================================
        while open_list_heap:
            # pop off open_list_heap and add it to closed_odict
            parent_node = heapq.heappop(open_list_heap)[2]
            closed_odict[parent_node] = True
            
            # check if we found the goal
            if parent_node == goal_node:
                solution_found = True
                break
            
            # check all possible moves
            for op_x, op_y in operations:
                child_node = parent_node[0] + op_x, parent_node[1] + op_y 
                movement_cost = self._calculate_actual_movement_cost(parent_node, (op_x, op_y))
                
                # check for invalid/bad move or if we already processed this node
                if movement_cost == None or child_node in closed_odict:
                    continue
                
                # calculate g(child_node) and update dictionaries and open list only if 
                # it's lower than a previous g or if we have never calculated it before
                child_g = gn_dict[parent_node] + movement_cost
                if child_g < gn_dict.get(child_node, math.inf):
                    # update dictionaries
                    path_dict[child_node] = parent_node
                    gn_dict[child_node] = child_g
                    child_h = self._heuristic(child_node, goal_node)
                    fn_dict[child_node] = child_g + child_h
                    
                    # update open list priority queue
                    heapq.heappush(open_list_heap, (fn_dict[child_node], child_h, child_node))
                    
                    # check if this is not the end of an attempted path
                    if len(open_list_heap) > 0 and open_list_heap[0][0] >= fn_dict[child_node]:
                        closed_odict[parent_node] = False
        # ============================================================================================================
        
        end_time = datetime.datetime.now()
        
        # print total time taken
        self._dynamic_info_text = self._dynamic_info_text + "[{:d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}.{:0004d}] Finished A* heuristic search!\n".format(
            end_time.year, end_time.month, end_time.day, end_time.hour, end_time.minute, end_time.second, end_time.microsecond
        )
        time_taken_in_sec = (end_time - true_start_time).total_seconds()
        self._dynamic_info_text = self._dynamic_info_text + "Search time : {:.5f} seconds\n".format(time_taken_in_sec)
        self._update_info_text()
        
        # check if we found the solution
        if not solution_found and start_node != goal_node:
            self._dynamic_info_text = self._dynamic_info_text + "Due to blocks, no path is found.\nPlease change the map and try again."
        elif time_taken_in_sec > 10:
            self._dynamic_info_text = self._dynamic_info_text + "Time is up. The optimal path is not found."
        else:
            # there is a solution and we found it in <= 10 sec
            # display total cost to goal
            self._dynamic_info_text = self._dynamic_info_text + "Total solution cost : {:.2f}".format(gn_dict.get(goal_node, 0))
            self._update_info_text()
            
            # extract solution path info from path_dict
            solution_list = []
            key_node = goal_node
            while key_node in path_dict:
                solution_list.append(key_node)
                key_node = path_dict[key_node]
            self._ai_solution_path.clear()
            self._ai_solution_path.extend(solution_list)
            
            if self._debug:
                if self._debug_options['draw_open_closed_list']:
                    # plot the nodes in the open and closed list
                    self._plot_points([p[2] for p in open_list_heap], self.colors[9], '.')
                    self._plot_points(closed_odict.keys(), self.colors[8], '.')
                
                if self._debug_options['draw_attempted_paths']:
                    # plot attempted paths
                    for k in closed_odict:
                        if closed_odict[k]:
                            attempted_path = []
                            n = k
                            while n in path_dict:
                                attempted_path.append(n)
                                if n in solution_list:
                                    break
                                n = path_dict[n]
                            self._plot_line(attempted_path, self.colors[8])
            
            # plot the solution path
            self._plot_line(self._ai_solution_path)
        
        self._update_info_text()


    """
        _heuristic
        h(n) : Uses diagonal distance to estimate cost without overestimating the actual cost

        Arguments:
            node_a {tuple} -- the source node
            node_b {tuple} -- the destination node

        Returns:
            float -- the estimated cost of going from source to destination node
    """
    def _heuristic(self, node_a: tuple, node_b: tuple) -> float:
        dx = abs(node_b[0] - node_a[0])
        dy = abs(node_b[1] - node_a[1])
        
        diagonal_cost = 1.5
        straight_cost = 1
        
        return straight_cost * (dx + dy) + (diagonal_cost - 2 * straight_cost) * min(dx, dy)


    """
        _calculate_actual_movement_cost
        calculates the actual movement cost of a single movement.

        Arguments:
            node_a {tuple} -- the source node
            operation {tuple} -- the operation to be performed on node_a

        Returns:
            float -- None if the operation is illegal or blocked, else the actual cost of going from source to next node
    """
    def _calculate_actual_movement_cost(self, node_a: tuple, operation: tuple) -> float:
        cost = 0
        
        # check if we are not moving
        if operation == (0, 0):
            return 0
        
        # check if we are trying to move outside the boundaries of the map
        node_b = (node_a[0] + operation[0], node_a[1] + operation[1])
        moving_out_of_bounds = bool(
            (node_b[0] > self._columns or node_b[0] < 0 or node_b[1] > self._rows or node_b[1] < 0) or
            ((operation == (0, 1) or operation == (0, -1)) and (node_a[0] <= 0 or node_a[0] >= self._columns)) or
            ((operation == (1, 0) or operation == (-1, 0)) and (node_a[1] <= 0 or node_a[1] >= self._rows))
        )
        if moving_out_of_bounds:
            return None
        
        # check if the path is blocked
        path_is_blocked = bool(
            (operation == (1, 1) and self._crime_grid[node_a[0], node_a[1]] >= self._crime_rate_treshold) or
            (operation == (1, -1) and self._crime_grid[node_a[0], node_a[1] - 1] >= self._crime_rate_treshold) or
            (operation == (-1, -1) and self._crime_grid[node_a[0] - 1, node_a[1] - 1] >= self._crime_rate_treshold) or
            (operation == (-1, 1) and self._crime_grid[node_a[0] - 1, node_a[1]] >= self._crime_rate_treshold) or
            (operation == (0, 1) and self._crime_grid[node_a[0] - 1, node_a[1]] >= self._crime_rate_treshold and self._crime_grid[node_a[0], node_a[1]] >= self._crime_rate_treshold) or
            (operation == (1, 0) and self._crime_grid[node_a[0], node_a[1]] >= self._crime_rate_treshold and self._crime_grid[node_a[0], node_a[1] - 1] >= self._crime_rate_treshold) or
            (operation == (0, -1) and self._crime_grid[node_a[0] - 1, node_a[1] - 1] >= self._crime_rate_treshold and self._crime_grid[node_a[0], node_a[1] - 1] >= self._crime_rate_treshold) or
            (operation == (-1, 0) and self._crime_grid[node_a[0] - 1, node_a[1] - 1] >= self._crime_rate_treshold and self._crime_grid[node_a[0] - 1, node_a[1]] >= self._crime_rate_treshold)
        )
        if path_is_blocked:
            cost = None
        else:
            # check if we are moving diagonally
            if operation == (1, 1) or operation == (1, -1) or operation == (-1, 1) or operation == (-1, -1):
                cost = 1.5
            else:
                # since we are not moving diagonally, check the blocks on each side of the edge to properly evaluate cost
                unblocked = 0
                if operation == (0, 1):
                    if self._crime_grid[node_a[0] - 1, node_a[1]] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                    if self._crime_grid[node_a[0], node_a[1]] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                elif operation == (1, 0):
                    if self._crime_grid[node_a[0], node_a[1]] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                    if self._crime_grid[node_a[0], node_a[1] - 1] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                elif operation == (0, -1):
                    if self._crime_grid[node_a[0] - 1, node_a[1] - 1] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                    if self._crime_grid[node_a[0], node_a[1] - 1] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                elif operation == (-1, 0):
                    if self._crime_grid[node_a[0] - 1, node_a[1] - 1] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                    if self._crime_grid[node_a[0] - 1, node_a[1]] < self._crime_rate_treshold:
                        unblocked = unblocked + 1
                
                if unblocked == 1:
                    cost = 1.3
                elif unblocked == 2:
                    cost = 1
        
        return cost


