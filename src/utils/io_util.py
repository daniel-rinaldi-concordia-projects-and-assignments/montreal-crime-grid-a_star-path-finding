# --------------------------------------------------------
# Assignment 1
# Written by Daniel Rinaldi 40010464
# For COMP 472 Section (your lab section) – Summer 2020
# --------------------------------------------------------

def prompt_menu(menu_name: str, options: list) -> int:
    choice = -1
    while True:
        print('\n=======================================\n', menu_name)
        print_big_brain()
        print("=======================================")
        for i in range(len(options)):
            print(i, ": ", options[i])
        
        choice = input("\nSelect an option: ").strip()
        if not choice.isdigit() or int(choice) < 0 or int(choice) > len(options)-1:
            print("Invalid choice ({}). Please enter an integer between 0 and {}.".format(choice, len(options)-1))
        else:
            print("=======================================")
            break
    
    return int(choice)


def prompt_confirm(s: str) -> bool:
    print(s, end=" ")
    choice = input("(y/n) : ").strip()
    return True if choice == "y" or choice == "yes" else False


def print_big_brain(line: int = -1):
        if line == 0 or line < 0: print("      _---~~(~~-_.       ", end="" if line >= 0 else '\n')
        if line == 1 or line < 0: print("    _{        )   )      ", end="" if line >= 0 else '\n')
        if line == 2 or line < 0: print("  ,   ) -~~- ( ,-' )_    ", end="" if line >= 0 else '\n')
        if line == 3 or line < 0: print(" (  `-,_..`., )-- '_,)   ", end="" if line >= 0 else '\n')
        if line == 4 or line < 0: print("( ` _)  (  -~( -_ `,  }  ", end="" if line >= 0 else '\n')
        if line == 5 or line < 0: print("(_-  _  ~_-~~~~`,  ,' )  ", end="" if line >= 0 else '\n')
        if line == 6 or line < 0: print("  `~ -^(    __;-,((()))  ", end="" if line >= 0 else '\n')
        if line == 7 or line < 0: print("        ~~~~ {_ -_(())   ", end="" if line >= 0 else '\n')
        if line == 8 or line < 0: print("               `\  }     ", end="" if line >= 0 else '\n')
        if line == 9 or line < 0: print("                 { }     ", end="" if line >= 0 else '\n')