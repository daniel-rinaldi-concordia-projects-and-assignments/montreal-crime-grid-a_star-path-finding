# --------------------------------------------------------
# Assignment 1
# Written by Daniel Rinaldi 40010464
# For COMP 472 Section JX – Summer 2020
# --------------------------------------------------------

import utils.io_util as io_util
import constants
from crime_grid import CrimeGrid

class App:
    SHAPE_FILE_PATH = constants.PROJECT_ROOT_DIR + "Shape/crime_dt"
    
    def __init__(self):
        self._running = False

    
    """
        start
        starts the app
    """
    def start(self):
        self._running = True
        self._loop()

    """
        _loop
        main application loop
    """
    def _loop(self):
        while self._running:
            try:
                self._process_input(io_util.prompt_menu("Main Menu", ["Quit", "Crime Grid"]))
            except Exception as e:
                print(e)
        
        self._on_shutdown()

    
    """
        quit
        signals the main application loop to end
    """
    def quit(self):
        self._running = False

    
    """
        _on_shutdown
        called when the app shuts down
    """
    def _on_shutdown(self):
        print("\nGoodbye.")


    """
        _process_input
        processes user input
        
        Arguments:
            choice {int} -- the integer choice that the user entered
    """
    def _process_input(self, choice: int):
        if choice == 0:
            self.quit()
        elif choice == 1:
            cell_size = App.prompt_cell_size()
            crime_grid = CrimeGrid()
            
            # Load the CrimeGrid. load function takes an optional 3rd argument, which would be the bounding box
            # crime_grid.load(App.SHAPE_FILE_PATH, cell_size, [-73.59, 45.49, -73.55, 45.53])
            crime_grid.load(App.SHAPE_FILE_PATH, cell_size)
            
            if crime_grid.is_loaded:
                crime_grid.open('COMP 472 Summer 2020 Assignment 1')
    

    """
        prompt_cell_size
        prompts user for a cell size needed to construct a CrimeGrid
        
        Returns:
            float -- the cell size that the user entered
    """
    def prompt_cell_size() -> float:
        cell_size = 0.00
        while True:
            choice = input("\nEnter a cell size (must be >= 0.001): ").strip()
            try:
                c = float(choice)
                if c >= 0.001:
                    cell_size = c
                    break
                else:
                    raise ValueError()
            except ValueError as e:
                print("invalid cell size, please input a value that is numeric and >= 0.001")
        
        return cell_size
    
    